#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bmp/headers/bmp_read.h"
#include "bmp/headers/bmp_write.h"
#include "rotate/headers/rotate.h"

int main(int argc, char **argv)
{
    if (argc < 3) {
        fprintf(stderr, "Not enough arguments");
        return 1;
    }

    struct image img = {0};
    struct image img_rt = {0};

    char *read_status = open_bmp(argv[1], &img);
    if (strcmp(read_status, "Success") != 0) {
        fprintf(stderr, "READING ERROR: %s\n", read_status);
        delete_image(&img);
        delete_image(&img_rt);
        return 1;
    }

    char *rotate_status = rotate_img(&img, &img_rt);
    if (strcmp(rotate_status, "Success") != 0) {
        fprintf(stderr, "ROTATING ERROR: %s\n", rotate_status);
        delete_image(&img);
        delete_image(&img_rt);
        return 1;
    }

    char *write_status = close_bmp(argv[2], &img_rt);
    if (strcmp(write_status, "Success") != 0) {
        fprintf(stderr, "WRITING ERROR: %s\n", write_status);
        delete_image(&img);
        delete_image(&img_rt);
        return 1;
    }

    delete_image(&img);
    delete_image(&img_rt);

    return 0;
}

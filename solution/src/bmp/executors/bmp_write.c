#include "../headers/bmp_write.h"

// Возвращает статус записи
char *get_write_status_msg(enum write_status status)
{
    if (status == FAIL_WRITE) return "Fail to write a new file";
    else return "Success";
}

struct bmp_header create_header(struct image *img) {
    struct bmp_header bmp_header = {0};
    // fix for padding bug
    size_t fix = img->width % 4 == 0 ? 0 : 4 - (img->width * 3) % 4;

    // header setting
    bmp_header.bfType = BF_TYPE;
    bmp_header.bfileSize = sizeof(struct bmp_header) + (img->width * 3 + fix) * img->height;
    bmp_header.bfileSize = 0;
    bmp_header.biSize = BI_SIZE;
    bmp_header.bOffBits = sizeof(struct bmp_header);
    bmp_header.biWidth = img->width;
    bmp_header.biHeight = img->height;
    bmp_header.biPlanes = BI_PLANES;
    bmp_header.biBitCount = BI_BIT_COUNT;
    bmp_header.biCompression = BI_COMPRESSION;
    bmp_header.biSizeImage = BI_SIZE_IMAGE;
    bmp_header.biXPelsPerMeter = BI_X_PELS_PER_METER;
    bmp_header.biYPelsPerMeter = BI_Y_PELS_PER_METER;
    bmp_header.biClrUsed = BI_CLR_IMPORTANT;
    bmp_header.biClrImportant = BI_CLR_IMPORTANT;

    return bmp_header;
}

// Пытается записать в файл
enum write_status to_bmp(FILE *out, struct image *img)
{
    struct bmp_header bmp_header = create_header(img);
    struct pixel *data = img->data;
    // fix for padding bug
    size_t fix = img->width % 4 == 0 ? 0 : 4 - (img->width * 3) % 4;

    if (fwrite(&bmp_header, sizeof(struct bmp_header), 1, out) != 1)
        return FAIL_WRITE;

    uint32_t to_write = bmp_header.biWidth * 3 + fix;
    for (uint32_t j = 0; j < bmp_header.biHeight - 1; j++)
    {
        if (fwrite(data, 1, to_write, out) - to_write != 0) return FAIL_WRITE;
        else data += bmp_header.biWidth;
    }
    to_write -= fix;
    if (fwrite(data, 1, to_write, out) - to_write != 0) return 0;

    return SUCCESSFUL_WRITE;
}

char *close_bmp(char *file_path, struct image *img)
{
    FILE *file = fopen(file_path, "wb");
    enum write_status status;
    if (file == NULL) status = FAIL_WRITE;
    else status = to_bmp(file, img);
    fclose(file);
    return get_write_status_msg(status);
}

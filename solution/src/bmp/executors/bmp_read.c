#include "../headers/bmp_read.h"

// Возвращает статус чтения
char *get_read_status_msg(enum read_status status)
{
    if (status == SUCCESSFUL_READ) return "Success";
    if (status == IO_FAIL_READ) return "IO Exception";
    if (status == CORRUPTED_FAIL_READ) return "Headers is incorrect";
    else return "Something was wrong";
}

// Читает хедер и проверяет на корректность
enum read_status read_header(FILE *in, struct bmp_header *bmp_header)
{
    size_t status = fread(bmp_header, sizeof(struct bmp_header), 1, in);

    if (status != 1) return IO_FAIL_READ;
    if (bmp_header->bfType != BF_TYPE) return CORRUPTED_FAIL_READ;
    if (bmp_header->biBitCount != BI_BIT_COUNT) return CORRUPTED_FAIL_READ;
    if (bmp_header->biPlanes != BI_PLANES) return CORRUPTED_FAIL_READ;
    if (bmp_header->biCompression != BI_COMPRESSION) return CORRUPTED_FAIL_READ;
    if (fseek(in, bmp_header->bOffBits, SEEK_SET) != 0) return IO_FAIL_READ;
    
    return SUCCESSFUL_READ;
}

// Читает пиксели
int read_pixels(FILE *in, struct image *img, struct bmp_header *bmp_header)
{
    // fix for padding bug
    size_t fix = bmp_header->biWidth % 4 == 0 ? 0 : 4 - (bmp_header->biWidth * 3) % 4;
    struct pixel *data = img->data;
    uint32_t to_read = bmp_header->biWidth * 3 + fix;

    for (uint32_t j = 0; j < bmp_header->biHeight - 1; j++)
    {
        if (fread(data, 1, to_read, in) - to_read != 0) return 0;
        else data += bmp_header->biWidth;
    }

    to_read -= fix;
    if (fread(data, 1, to_read, in) - to_read != 0) return 0;
    return 1;
}

// Пытается прочитать файл
enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header bmp_header;
    enum read_status header_status = read_header(in, &bmp_header);

    if (header_status != SUCCESSFUL_READ) return header_status;
    create_image(img, bmp_header.biWidth, bmp_header.biHeight);
    if (read_pixels(in, img, &bmp_header) == 0) return UNEXPECTED_READ;

    return SUCCESSFUL_READ;
}

char *open_bmp(char *file_path, struct image *img)
{
    printf("%s", file_path);
    FILE *file = fopen(file_path, "rb");
    enum read_status status;
    if (file == NULL) status = IO_FAIL_READ;
    else status = from_bmp(file, img);

    fclose(file);
    return get_read_status_msg(status);
}

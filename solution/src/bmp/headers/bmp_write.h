#ifndef BMP_WRITE_H
#define BMP_WRITE_H

#include "bmp.h"

enum write_status
{
    SUCCESSFUL_WRITE = 0,
    FAIL_WRITE,
};

// Возвращает статус записи
char *get_write_status_msg(enum write_status status);

// Пытается записать в файл
enum write_status to_bmp(FILE *out, struct image *img);

char *close_bmp(char *file_path, struct image *img);

#endif

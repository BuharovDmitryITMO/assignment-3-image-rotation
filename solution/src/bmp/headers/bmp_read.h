#ifndef BMP_READ_H
#define BMP_READ_H

#include "bmp.h"


enum read_status
{
    SUCCESSFUL_READ = 0,
    IO_FAIL_READ,
    CORRUPTED_FAIL_READ,
    UNEXPECTED_READ,
};

// Возвращает статус чтения
char *get_read_status_msg(enum read_status status);

// Пытается прочитать файл
enum read_status from_bmp(FILE *in, struct image *img);

// Открывает файл
char *open_bmp(char *file_path, struct image *img);

#endif

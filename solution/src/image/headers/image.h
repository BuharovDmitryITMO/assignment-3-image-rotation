#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>

// Структура хранения пикселей
struct pixel
{
  uint8_t b, g, r;
};

// Структура картинки
struct image
{
  uint64_t width, height;
  struct pixel *data;
};

// Инициализация картинки
int create_image(struct image *img, uint64_t width, uint64_t height);

// Получить пиксель с картинки
struct pixel get_pixel_by_index(struct image *img, uint64_t i, uint64_t j);

// Установить пиксель
void set_pixel_by_index(struct image *img, struct pixel pixel, uint64_t i, uint64_t j);

// Очистить память
void delete_image(struct image *img);

#endif

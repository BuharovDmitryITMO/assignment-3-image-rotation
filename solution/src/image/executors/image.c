#include "../headers/image.h"

// Инициализация картинки
int create_image(struct image *img, uint64_t width, uint64_t height)
{
    img->width = width;
    img->height = height;
    img->data = malloc(width * height * sizeof(struct pixel));
    if (img->data == 0)
    {
        fprintf(stderr, "Not enough memory");
        return 1;
    }
    return 0;
}

// Получить пиксель с картинки
struct pixel get_pixel_by_index(struct image *img, uint64_t i, uint64_t j)
{
    return img->data[img->width * j + i];
}

// Установить пиксель
void set_pixel_by_index(struct image *img, struct pixel pixel, uint64_t i, uint64_t j)
{
    img->data[img->width * j + i] = pixel;
}

// Очистить память
void delete_image(struct image *img)
{
    free(img->data);
}

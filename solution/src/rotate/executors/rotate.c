#include "../headers/rotate.h"

// Возвращает статус поворота
char *get_rotate_status_msg(enum rotate_status status)
{
    if (status == SUCCESSFUL_ROTATE) return "Success";
    else return "Something was wrong";
}

char *rotate_img(struct image *img, struct image *img_rt)
{
    uint32_t width = img->height;
    uint32_t height = img->width;
    if (create_image(img_rt, width, height) == 1) return get_rotate_status_msg(FAIL_ROTATE);
    
    for (uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j = 0; j < width; j++)
        {
            set_pixel_by_index(img_rt, get_pixel_by_index(img, i, j), width - j - 1, i);
        }
    }
    return get_rotate_status_msg(SUCCESSFUL_ROTATE);
}

#ifndef ROTATE_H
#define ROTATE_H

#include "../../image/headers/image.h"

enum rotate_status
{
    SUCCESSFUL_ROTATE = 0,
    FAIL_ROTATE
};

char *rotate_img(struct image *img, struct image *img_rt);

#endif
